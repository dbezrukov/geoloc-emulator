Number.prototype.toRad = function() {
  return this * Math.PI / 180;
}

Number.prototype.toDeg = function() {
  return this * 180 / Math.PI;
}

module.exports = { 
	
	destinationPoint : function(lat, lon, brng, radius) {

	  	radius = radius / 6371;  
	  	brng = brng.toRad();  

	  	var lat1 = lat.toRad();
	  	var lon1 = lon.toRad();

	  	var lat2 = Math.asin(Math.sin(lat1) * Math.cos(radius) + 
	                       Math.cos(lat1) * Math.sin(radius) * Math.cos(brng));

	  	var lon2 = lon1 + Math.atan2(Math.sin(brng) * Math.sin(radius) *
	                               	Math.cos(lat1), 
	                               	Math.cos(radius) - Math.sin(lat1) *
	                               	Math.sin(lat2));

	  	if (isNaN(lat2) || isNaN(lon2)) return null;

	  	return {
	  		lat: lat2.toDeg(),
	  		lon: lon2.toDeg()
		}
	},

	// speed [km/h]
	// radius [km]
	// elapsed [sec]
	// returns bearing [grad]
	nextBrng: function(V, R, t) {
		var w = V / R;
		var fi_grad = w * t;

		return fi_grad.toDeg() % 360.;
	}
}