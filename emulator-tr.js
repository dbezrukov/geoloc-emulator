var http = require('http');

function post(path, dataString, cookies, callback) {
	var headers = {
		'Content-Type': 'application/json',
		'Content-Length': dataString.length,
		'Cookie': cookies
	};

	var options = {
		host: 'localhost',
		port: 3021,
		path: path,
		method: 'POST',
		headers: headers
	};

	var req = http.request(options, function(res) {

    	cookies = res.headers["set-cookie"];

		res.on('data', function (chunk) {
      	});
       	res.on('end', function () {
       		callback(null, cookies);
  		});
	});

	req.on('error', function (err) {
		callback(err);
	});

	req.write(dataString);
	req.end();
}

function start() {
	var self = this;

	var loginData = {
		email: self.email,
		password: self.password
	}
	
	console.log('Logging under ' + self.email);

	post('/auth/local/login', JSON.stringify(loginData), '', function(err, cookies) {

		if (!err) {

			console.log('Login ok');

			function random(low, high) {
    			return Math.random() * (high - low) + low;
			}

			setInterval(function() {
				var rand = random(0.00001, 0.01);

				var locationData = {
					data: {
						pos: {
							latitude: self.lat + rand,
							longitude: self.lon + rand
						},
						timestamp: Date.now()
					}
				}

				console.log('Positng new location for ' + self.email);

				post('/data/location', JSON.stringify(locationData), cookies, function(err, cookies) {

				});

			}, self.interval);

		} else {
			console.log('Login failed with reason ' + err);
			console.log('Reconnecting in 3 seconds..');
			
			setTimeout(function() {
				start.call(self);
			}, 3000);
		}
	});
}

console.log('Starting emulator');

process.on('uncaughtException', function(err) {
    console.log(err);
});

start.call({ email: 'bot1@teamradar.org', password: 'bot1', lat: 60.11822, lon: 30.18768, interval: 6000  });
start.call({ email: 'bot2@teamradar.org', password: 'bot2', lat: 60.10944, lon: 30.20227, interval: 15000 });
start.call({ email: 'bot3@teamradar.org', password: 'bot3', lat: 60.12394, lon: 30.21086, interval: 7000  });
start.call({ email: 'bot4@teamradar.org', password: 'bot4', lat: 60.12394, lon: 30.21986, interval: 10000 });
start.call({ email: 'bot5@teamradar.org', password: 'bot5', lat: 60.11486, lon: 30.21412, interval: 16000 });
start.call({ email: 'bot6@teamradar.org', password: 'bot6', lat: 60.11522, lon: 30.18668, interval: 10000 });
start.call({ email: 'bot7@teamradar.org', password: 'bot7', lat: 60.10744, lon: 30.20227, interval: 5000 });
start.call({ email: 'bot8@teamradar.org', password: 'bot8', lat: 60.12194, lon: 30.21186, interval: 8000 });
start.call({ email: 'bot9@teamradar.org', password: 'bot9', lat: 60.12594, lon: 30.21986, interval: 5000  });
