var _ = require('underscore');
var sprintf = require("sprintf-js").sprintf,
    vsprintf = require("sprintf-js").vsprintf

var dgram = require('dgram');
var socket = dgram.createSocket('udp4');

var tools = require('./emulator-tools');

function sendTelemetry(data, callback) {

	console.log('sendTelemetry');

	var string = vsprintf(
		'#%s$%s,%f,%f,%s,%f,%f,%f,%f,%f,%f,%s\r\n', [
			data.board,
		 	data.type,
		 	data.lat,
			data.lon,
			data.source === 'gps' ? data.utc : '',
			data.hgt,
			data.pch,
			data.roll,
			data.yaw,
			data.hdg,
			data.speed,
			data.source,
		]
	)

	var message = Buffer.from(string);

	socket.send(message, 48888, 'okocenter.ru', function(err) {
		if (err) {
			console.log(err);
		}
	});
}

function start() {
	var self = this;

	self.started = Date.now();

	setInterval(function() {
		self.now = Date.now();

		self.brng = tools.nextBrng(self.speed / 3600., self.radius, ( self.now - self.started ) / 1000.);
		self.pos = tools.destinationPoint(self.lat, self.lon, self.brng, self.radius);

		var telemetryData = {
			board: self.board,
			type: self.type,
			lat: self.pos.lat,
			lon: self.pos.lon,
			utc: self.now,
			hgt: self.hgt,
			pch: 0.12323523,
			roll: 0.2351235,
			yaw: 0.235235125,
			hdg:  (self.brng + 90.) % 360.,
			speed: self.speed,
			source: self.source,
		}

		sendTelemetry(telemetryData, function(err) {
			if (err) {
				console.log('sendTelemetry error: ' + err);
			}
		});

	}, 1000);
}

console.log('Starting emulator');

process.on('uncaughtException', function(err) {
    console.log(err);
});

// speed [km/h]
// radius [km]
start.call({ board: '50500', name: 'oko', password: 'okocenter', type: 'uav',
			 lat: 60.55145, lon: 30.3338513, speed: 120, radius: 1.5, hgt: 300, source: 'gps' });

start.call({ board: '50501', name: 'oko', password: 'okocenter', type: 'uav',
			 lat: 60.55145, lon: 30.3338513, speed: 100, radius: 2.3, hgt: 250, source: 'network' });