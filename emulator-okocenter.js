var _ = require('underscore');
var http = require('https');
var fs = require('fs');

var tools = require('./emulator-tools');

function post(path, dataString, cookies, callback) {

	var headers = {
		'Content-Type': 'application/json',
		'Content-Length': dataString.length,
		'Cookie': cookies
	};

	var options = {
		hostname: 'map.okocenter.ru',
		port: 443,
		path: path,
		method: 'POST',
		headers: headers
	};

	var req = http.request(options, function(res) {

    	cookies = res.headers["set-cookie"];

		res.on('data', function (chunk) {
      	});
       	res.on('end', function () {
       		callback(null, cookies);
  		});
	});

	req.on('error', function (err) {
		callback(err);
	});

	req.write(dataString);
	req.end();
}

function postVideo(path, dataString, filePath, cookies, callback) {

	var stat = fs.statSync(filePath);

	var headers = {
		'Content-Type': 'image/jpeg',
		'Content-Length': stat.size,
		'Cookie': cookies,
		'telemetry': dataString
	};

	var options = {
		hostname: 'map.okocenter.ru',
		port: 443,
		path: path,
		method: 'POST',
		headers: headers
	};

	var req = http.request(options, function(res) {

    	cookies = res.headers["set-cookie"];
       	res.on('end', function () {
       		callback(null, cookies);
  		});
	});

	req.on('error', function (err) {
		callback(err);
	});

	fs.createReadStream(filePath).pipe(req);
}

function start() {
	var self = this;

	var loginData = {
		name: self.name,
		password: self.password
	}
	
	console.log('Logging under ' + self.name);

	post('/login2', JSON.stringify(loginData), '', function(err, cookies) {

		if (!err) {

			console.log('Login ok');

			self.started = Date.now();

			var postTelemetry = _.throttle(post, 1000);

			fs.readdir(self.videoDir + '/' + self.board, function (err, files) { 
				if (err) {
					console.log('error: ' + err);
					return;
				}

				if (files.length === 0) {
					console.log('no video files');
					return;
				}

				var fileIndex = 0;

				setInterval(function() {
					self.now = Date.now();

					self.brng = tools.nextBrng(self.speed / 3600., self.radius, ( self.now - self.started ) / 1000.);
					self.pos = tools.destinationPoint(self.lat, self.lon, self.brng, self.radius);

					var telemetryData = {
						board: self.board,
						type: self.type,
						lat: self.pos.lat,
						lon: self.pos.lon,
						utc: self.now,
						hgt: self.hgt,
						pch: 0.12323523,
						roll: 0.2351235,
						yaw: 0.235235125,
						hdg:  (self.brng + 90.) % 360.,
						speed: self.speed,
						source: 'gps'
					}

					postTelemetry('/board/' + self.board + '/telemetry', 
						JSON.stringify(telemetryData), cookies, 
						function(err, cookies) {
							if (err) {
								console.log('postTelemetry error: ' + err);
							}
					});

					var fileName = files[fileIndex++] || files[fileIndex = 0];

					postVideo('/board/' + self.board + '/video0', 
						JSON.stringify(telemetryData), 
						self.videoDir + '/' + self.board + '/' + fileName,
						cookies, 
						function(err, cookies) {
							if (err) {
								console.log('postVideo error: ' + err);
							}
					});

				}, 1000 / self.fps);

			});

		} else {
			console.log('Login failed with reason ' + err);
			console.log('Reconnecting in 3 seconds..');
			
			setTimeout(function() {
				start.call(self);
			}, 3000);
		}
	});
}

console.log('Starting emulator');

process.on('uncaughtException', function(err) {
    console.log(err);
});

// speed [km/h]
// radius [km]
start.call({ board: '1200', name: 'oko', password: 'okocenter', type: 'uav',
			 lat: 60.785311, lon: 29.937744, speed: 150, radius: 1, hgt: 400,
			 videoDir: global.process.env.VIDEODIR, fps: global.process.env.FPS });

start.call({ board: '1201', name: 'oko', password: 'okocenter', type: 'uav',
			 lat: 60.785311, lon: 29.937744, speed: 180, radius: 2, hgt: 300, 
			 videoDir: global.process.env.VIDEODIR, fps: global.process.env.FPS });

start.call({ board: '120', name: 'oko', password: 'okocenter', type: 'vehicle',
			 lat: 60.66981, lon: 30.325012, speed: 30, radius: 3, hgt: 0, 
			 videoDir: global.process.env.VIDEODIR, fps: global.process.env.FPS });

start.call({ board: '140', name: 'oko', password: 'okocenter', type: 'boat',
			 lat: 60.76721, lon: 30.72533, speed: 20, radius: 2, hgt: 0, 
			 videoDir: global.process.env.VIDEODIR, fps: global.process.env.FPS });

start.call({ board: '160', name: 'oko', password: 'okocenter', type: 'man',
			 lat: 60.578198, lon: 30.11764, speed: 5, radius: 0.5, hgt: 0, 
			 videoDir: global.process.env.VIDEODIR, fps: global.process.env.FPS });
